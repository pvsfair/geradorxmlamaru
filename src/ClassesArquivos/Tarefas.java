/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

import java.util.ArrayList;

/**
 *
 * @author ICCAF
 */
public class Tarefas {
    ArrayList<Tarefa> arrayList;

    public Tarefas() {
        this.arrayList = new ArrayList<>();
    }

    public ArrayList<Tarefa> getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList<Tarefa> arrayList) {
        this.arrayList = arrayList;
    }
}

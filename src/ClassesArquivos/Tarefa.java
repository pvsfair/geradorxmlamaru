/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

import java.util.ArrayList;

/**
 *
 * @author ICCAF
 */
public class Tarefa {

    private ArrayList<idEscolha> escolhas;
    private int Id;
    private int MiniGame;
    private int CompareNumber;
    private int CorrectChoice;
    private int TaskType;
    private int Model;
    private int TreinoOuTeste;

    public Tarefa(ArrayList<idEscolha> escolhas, int Id, int MiniGame, int CorrectChoice, int TaskType, int Model, int TreinoOuTeste) {
        this.escolhas = escolhas;
        this.Id = Id;
        this.MiniGame = MiniGame;
        this.CompareNumber = escolhas.size();
        this.CorrectChoice = CorrectChoice;
        this.TaskType = TaskType;
        this.Model = Model;
        this.TreinoOuTeste = TreinoOuTeste;
    }

    public ArrayList<idEscolha> getEscolhas() {
        return escolhas;
    }

    public void setEscolhas(ArrayList<idEscolha> escolhas) {
        this.escolhas = escolhas;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public int getMiniGame() {
        return MiniGame;
    }

    public void setMiniGame(int MiniGame) {
        this.MiniGame = MiniGame;
    }

    public int getCompareNumber() {
        return CompareNumber;
    }

    public void setCompareNumber(int CompareNumber) {
        this.CompareNumber = CompareNumber;
    }

    public int getCorrectChoice() {
        return CorrectChoice;
    }

    public void setCorrectChoice(int CorrectChoice) {
        this.CorrectChoice = CorrectChoice;
    }

    public int getTaskType() {
        return TaskType;
    }

    public void setTaskType(int TaskType) {
        this.TaskType = TaskType;
    }

    public int getModel() {
        return Model;
    }

    public void setModel(int Model) {
        this.Model = Model;
    }

    public int getTreinoOuTeste() {
        return TreinoOuTeste;
    }

    public void setTreinoOuTeste(int TreinoOuTeste) {
        this.TreinoOuTeste = TreinoOuTeste;
    }
    
}

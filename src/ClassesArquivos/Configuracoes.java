/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

/**
 *
 * @author ICCAF
 */
public class Configuracoes {
    private int numero_De_Tarefas_A_Serem_Geradas;
    private boolean utilizar_Algoritmo_de_Proximidade;
    private float peso_Alfa_Leitura;
    private float peso_Beta_Leitura;
    private float peso_Gama_Leitura;
    private float peso_Alfa_Escrita;
    private float peso_Beta_Escrita;
    private float peso_Gama_Escrita;
    private float peso_Sigma_Escrita;
    private int numero_de_Escolhas_em_uma_Tarefa;
    private int alfa_Maquina_de_Aprendizado;
    private int numero_De_Interacoes_da_Maquina_de_Aprendizado;
    private float Peso_Do_Alg_Prox_Da_Regra1;
    private float Peso_Do_Alg_Prox_Da_Regra2;
    private float Peso_Do_Alg_Prox_Da_Regra3;
    private float Peso_Do_Alg_Prox_Da_Regra4;
    private float Peso_Do_Alg_Prox_Da_Regra5;
    private float Percentagem_Da_Taxa_De_Acerto;
    private float Percentagem_Da_Media_Geral;
    private float Percentagem_Da_Probabilidade_De_Acerto;

    public Configuracoes() {
        this.numero_De_Tarefas_A_Serem_Geradas = 20;
        this.utilizar_Algoritmo_de_Proximidade = false;
        this.peso_Alfa_Leitura = 0.25f;
        this.peso_Beta_Leitura = 0.25f;
        this.peso_Gama_Leitura = 0.25f;
        this.peso_Alfa_Escrita = 0.3f;
        this.peso_Beta_Escrita = 0.2f;
        this.peso_Gama_Escrita = 0.2f;
        this.peso_Sigma_Escrita = 0.3f;
        this.numero_de_Escolhas_em_uma_Tarefa = 5;
        this.alfa_Maquina_de_Aprendizado = 8;
        this.numero_De_Interacoes_da_Maquina_de_Aprendizado = 500;
        this.Peso_Do_Alg_Prox_Da_Regra1 = 0.2f;
        this.Peso_Do_Alg_Prox_Da_Regra2 = 0.225f;
        this.Peso_Do_Alg_Prox_Da_Regra3 = 0.175f;
        this.Peso_Do_Alg_Prox_Da_Regra4 = 0.1f;
        this.Peso_Do_Alg_Prox_Da_Regra5 = 0.3f;
        this.Percentagem_Da_Taxa_De_Acerto = 0.4f;
        this.Percentagem_Da_Media_Geral = 0.4f;
        this.Percentagem_Da_Probabilidade_De_Acerto = 0.4f;
    }

    
    
    /**
     * @return the numero_De_Tarefas_A_Serem_Geradas
     */
    public int getNumero_De_Tarefas_A_Serem_Geradas() {
        return numero_De_Tarefas_A_Serem_Geradas;
    }

    /**
     * @param numero_De_Tarefas_A_Serem_Geradas the numero_De_Tarefas_A_Serem_Geradas to set
     */
    public void setNumero_De_Tarefas_A_Serem_Geradas(int numero_De_Tarefas_A_Serem_Geradas) {
        this.numero_De_Tarefas_A_Serem_Geradas = numero_De_Tarefas_A_Serem_Geradas;
    }

    /**
     * @return the utilizar_Algoritmo_de_Proximidade
     */
    public boolean isUtilizar_Algoritmo_de_Proximidade() {
        return utilizar_Algoritmo_de_Proximidade;
    }

    /**
     * @param utilizar_Algoritmo_de_Proximidade the utilizar_Algoritmo_de_Proximidade to set
     */
    public void setUtilizar_Algoritmo_de_Proximidade(boolean utilizar_Algoritmo_de_Proximidade) {
        this.utilizar_Algoritmo_de_Proximidade = utilizar_Algoritmo_de_Proximidade;
    }

    /**
     * @return the peso_Alfa_Leitura
     */
    public float getPeso_Alfa_Leitura() {
        return peso_Alfa_Leitura;
    }

    /**
     * @param peso_Alfa_Leitura the peso_Alfa_Leitura to set
     */
    public void setPeso_Alfa_Leitura(float peso_Alfa_Leitura) {
        this.peso_Alfa_Leitura = peso_Alfa_Leitura;
    }

    /**
     * @return the peso_Beta_Leitura
     */
    public float getPeso_Beta_Leitura() {
        return peso_Beta_Leitura;
    }

    /**
     * @param peso_Beta_Leitura the peso_Beta_Leitura to set
     */
    public void setPeso_Beta_Leitura(float peso_Beta_Leitura) {
        this.peso_Beta_Leitura = peso_Beta_Leitura;
    }

    /**
     * @return the peso_Gama_Leitura
     */
    public float getPeso_Gama_Leitura() {
        return peso_Gama_Leitura;
    }

    /**
     * @param peso_Gama_Leitura the peso_Gama_Leitura to set
     */
    public void setPeso_Gama_Leitura(float peso_Gama_Leitura) {
        this.peso_Gama_Leitura = peso_Gama_Leitura;
    }

    /**
     * @return the peso_Alfa_Escrita
     */
    public float getPeso_Alfa_Escrita() {
        return peso_Alfa_Escrita;
    }

    /**
     * @param peso_Alfa_Escrita the peso_Alfa_Escrita to set
     */
    public void setPeso_Alfa_Escrita(float peso_Alfa_Escrita) {
        this.peso_Alfa_Escrita = peso_Alfa_Escrita;
    }

    /**
     * @return the peso_Beta_Escrita
     */
    public float getPeso_Beta_Escrita() {
        return peso_Beta_Escrita;
    }

    /**
     * @param peso_Beta_Escrita the peso_Beta_Escrita to set
     */
    public void setPeso_Beta_Escrita(float peso_Beta_Escrita) {
        this.peso_Beta_Escrita = peso_Beta_Escrita;
    }

    /**
     * @return the peso_Gama_Escrita
     */
    public float getPeso_Gama_Escrita() {
        return peso_Gama_Escrita;
    }

    /**
     * @param peso_Gama_Escrita the peso_Gama_Escrita to set
     */
    public void setPeso_Gama_Escrita(float peso_Gama_Escrita) {
        this.peso_Gama_Escrita = peso_Gama_Escrita;
    }

    /**
     * @return the peso_Sigma_Escrita
     */
    public float getPeso_Sigma_Escrita() {
        return peso_Sigma_Escrita;
    }

    /**
     * @param peso_Sigma_Escrita the peso_Sigma_Escrita to set
     */
    public void setPeso_Sigma_Escrita(float peso_Sigma_Escrita) {
        this.peso_Sigma_Escrita = peso_Sigma_Escrita;
    }

    /**
     * @return the numero_de_Escolhas_em_uma_Tarefa
     */
    public int getNumero_de_Escolhas_em_uma_Tarefa() {
        return numero_de_Escolhas_em_uma_Tarefa;
    }

    /**
     * @param numero_de_Escolhas_em_uma_Tarefa the numero_de_Escolhas_em_uma_Tarefa to set
     */
    public void setNumero_de_Escolhas_em_uma_Tarefa(int numero_de_Escolhas_em_uma_Tarefa) {
        this.numero_de_Escolhas_em_uma_Tarefa = numero_de_Escolhas_em_uma_Tarefa;
    }

    /**
     * @return the alfa_Maquina_de_Aprendizado
     */
    public int getAlfa_Maquina_de_Aprendizado() {
        return alfa_Maquina_de_Aprendizado;
    }

    /**
     * @param alfa_Maquina_de_Aprendizado the alfa_Maquina_de_Aprendizado to set
     */
    public void setAlfa_Maquina_de_Aprendizado(int alfa_Maquina_de_Aprendizado) {
        this.alfa_Maquina_de_Aprendizado = alfa_Maquina_de_Aprendizado;
    }

    /**
     * @return the numero_De_Interacoes_da_Maquina_de_Aprendizado
     */
    public int getNumero_De_Interacoes_da_Maquina_de_Aprendizado() {
        return numero_De_Interacoes_da_Maquina_de_Aprendizado;
    }

    /**
     * @param numero_De_Interacoes_da_Maquina_de_Aprendizado the numero_De_Interacoes_da_Maquina_de_Aprendizado to set
     */
    public void setNumero_De_Interacoes_da_Maquina_de_Aprendizado(int numero_De_Interacoes_da_Maquina_de_Aprendizado) {
        this.numero_De_Interacoes_da_Maquina_de_Aprendizado = numero_De_Interacoes_da_Maquina_de_Aprendizado;
    }

    /**
     * @return the Peso_Do_Alg_Prox_Da_Regra1
     */
    public float getPeso_Do_Alg_Prox_Da_Regra1() {
        return Peso_Do_Alg_Prox_Da_Regra1;
    }

    /**
     * @param Peso_Do_Alg_Prox_Da_Regra1 the Peso_Do_Alg_Prox_Da_Regra1 to set
     */
    public void setPeso_Do_Alg_Prox_Da_Regra1(float Peso_Do_Alg_Prox_Da_Regra1) {
        this.Peso_Do_Alg_Prox_Da_Regra1 = Peso_Do_Alg_Prox_Da_Regra1;
    }

    /**
     * @return the Peso_Do_Alg_Prox_Da_Regra2
     */
    public float getPeso_Do_Alg_Prox_Da_Regra2() {
        return Peso_Do_Alg_Prox_Da_Regra2;
    }

    /**
     * @param Peso_Do_Alg_Prox_Da_Regra2 the Peso_Do_Alg_Prox_Da_Regra2 to set
     */
    public void setPeso_Do_Alg_Prox_Da_Regra2(float Peso_Do_Alg_Prox_Da_Regra2) {
        this.Peso_Do_Alg_Prox_Da_Regra2 = Peso_Do_Alg_Prox_Da_Regra2;
    }

    /**
     * @return the Peso_Do_Alg_Prox_Da_Regra3
     */
    public float getPeso_Do_Alg_Prox_Da_Regra3() {
        return Peso_Do_Alg_Prox_Da_Regra3;
    }

    /**
     * @param Peso_Do_Alg_Prox_Da_Regra3 the Peso_Do_Alg_Prox_Da_Regra3 to set
     */
    public void setPeso_Do_Alg_Prox_Da_Regra3(float Peso_Do_Alg_Prox_Da_Regra3) {
        this.Peso_Do_Alg_Prox_Da_Regra3 = Peso_Do_Alg_Prox_Da_Regra3;
    }

    /**
     * @return the Peso_Do_Alg_Prox_Da_Regra4
     */
    public float getPeso_Do_Alg_Prox_Da_Regra4() {
        return Peso_Do_Alg_Prox_Da_Regra4;
    }

    /**
     * @param Peso_Do_Alg_Prox_Da_Regra4 the Peso_Do_Alg_Prox_Da_Regra4 to set
     */
    public void setPeso_Do_Alg_Prox_Da_Regra4(float Peso_Do_Alg_Prox_Da_Regra4) {
        this.Peso_Do_Alg_Prox_Da_Regra4 = Peso_Do_Alg_Prox_Da_Regra4;
    }

    /**
     * @return the Peso_Do_Alg_Prox_Da_Regra5
     */
    public float getPeso_Do_Alg_Prox_Da_Regra5() {
        return Peso_Do_Alg_Prox_Da_Regra5;
    }

    /**
     * @param Peso_Do_Alg_Prox_Da_Regra5 the Peso_Do_Alg_Prox_Da_Regra5 to set
     */
    public void setPeso_Do_Alg_Prox_Da_Regra5(float Peso_Do_Alg_Prox_Da_Regra5) {
        this.Peso_Do_Alg_Prox_Da_Regra5 = Peso_Do_Alg_Prox_Da_Regra5;
    }

    /**
     * @return the Percentagem_Da_Taxa_De_Acerto
     */
    public float getPercentagem_Da_Taxa_De_Acerto() {
        return Percentagem_Da_Taxa_De_Acerto;
    }

    /**
     * @param Percentagem_Da_Taxa_De_Acerto the Percentagem_Da_Taxa_De_Acerto to set
     */
    public void setPercentagem_Da_Taxa_De_Acerto(float Percentagem_Da_Taxa_De_Acerto) {
        this.Percentagem_Da_Taxa_De_Acerto = Percentagem_Da_Taxa_De_Acerto;
    }

    /**
     * @return the Percentagem_Da_Media_Geral
     */
    public float getPercentagem_Da_Media_Geral() {
        return Percentagem_Da_Media_Geral;
    }

    /**
     * @param Percentagem_Da_Media_Geral the Percentagem_Da_Media_Geral to set
     */
    public void setPercentagem_Da_Media_Geral(float Percentagem_Da_Media_Geral) {
        this.Percentagem_Da_Media_Geral = Percentagem_Da_Media_Geral;
    }

    /**
     * @return the Percentagem_Da_Probabilidade_De_Acerto
     */
    public float getPercentagem_Da_Probabilidade_De_Acerto() {
        return Percentagem_Da_Probabilidade_De_Acerto;
    }

    /**
     * @param Percentagem_Da_Probabilidade_De_Acerto the Percentagem_Da_Probabilidade_De_Acerto to set
     */
    public void setPercentagem_Da_Probabilidade_De_Acerto(float Percentagem_Da_Probabilidade_De_Acerto) {
        this.Percentagem_Da_Probabilidade_De_Acerto = Percentagem_Da_Probabilidade_De_Acerto;
    }
    
}

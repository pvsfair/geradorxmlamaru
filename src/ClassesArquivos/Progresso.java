/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

/**
 *
 * @author ICCAF
 */
public class Progresso {
    private int id_Da_Ultima_Tarefa;
    private int quantidade_Parafusos;

    public Progresso() {
        this.id_Da_Ultima_Tarefa = 1;
        this.quantidade_Parafusos = 0;
    }

    /**
     * @return the id_Da_Ultima_Tarefa
     */
    public int getId_Da_Ultima_Tarefa() {
        return id_Da_Ultima_Tarefa;
    }

    /**
     * @param id_Da_Ultima_Tarefa the id_Da_Ultima_Tarefa to set
     */
    public void setId_Da_Ultima_Tarefa(int id_Da_Ultima_Tarefa) {
        this.id_Da_Ultima_Tarefa = id_Da_Ultima_Tarefa;
    }

    /**
     * @return the quantidade_Parafusos
     */
    public int getQuantidade_Parafusos() {
        return quantidade_Parafusos;
    }

    /**
     * @param quantidade_Parafusos the quantidade_Parafusos to set
     */
    public void setQuantidade_Parafusos(int quantidade_Parafusos) {
        this.quantidade_Parafusos = quantidade_Parafusos;
    }
}

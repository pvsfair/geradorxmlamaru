/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

import java.util.ArrayList;

/**
 *
 * @author ICCAF
 */
public class Escolha {
    private ArrayList<Silaba> Syllables;
    private int WordId;
    private String Name;
    private String Resource;
    private int SyllablesNumber;
    private int MaxReadDif;
    private int LearningDegreeRead;
    private int LearnedRead;
    private int MaxWriteDif;
    private int LearningDegreeWrite;
    private int LearnedWrite;

    public Escolha(ArrayList<Silaba> Syllables, int WordId, String Name, String Resource) {
        this.Syllables = Syllables;
        this.WordId = WordId;
        this.Name = Name;
        this.Resource = Resource;
        this.SyllablesNumber = Syllables.size();
        this.MaxReadDif = 0;
        this.LearningDegreeRead = 0;
        this.LearnedRead = 0;
        this.MaxWriteDif = 0;
        this.LearningDegreeWrite = 0;
        this.LearnedWrite = 0;
    }
    
    public ArrayList<Silaba> getSyllables() {
        return Syllables;
    }

    public void setSyllables(ArrayList<Silaba> Syllables) {
        this.Syllables = Syllables;
    }

    public int getWordId() {
        return WordId;
    }

    public void setWordId(int WordId) {
        this.WordId = WordId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getResource() {
        return Resource;
    }

    public void setResource(String Resource) {
        this.Resource = Resource;
    }

    public int getSyllablesNumber() {
        return SyllablesNumber;
    }

    public void setSyllablesNumber(int SyllablesNumber) {
        this.SyllablesNumber = SyllablesNumber;
    }

    public int getMaxReadDif() {
        return MaxReadDif;
    }

    public void setMaxReadDif(int MaxReadDif) {
        this.MaxReadDif = MaxReadDif;
    }

    public int getLearningDegreeRead() {
        return LearningDegreeRead;
    }

    public void setLearningDegreeRead(int LearningDegreeRead) {
        this.LearningDegreeRead = LearningDegreeRead;
    }

    public int getLearnedRead() {
        return LearnedRead;
    }

    public void setLearnedRead(int LearnedRead) {
        this.LearnedRead = LearnedRead;
    }

    public int getMaxWriteDif() {
        return MaxWriteDif;
    }

    public void setMaxWriteDif(int MaxWriteDif) {
        this.MaxWriteDif = MaxWriteDif;
    }

    public int getLearningDegreeWrite() {
        return LearningDegreeWrite;
    }

    public void setLearningDegreeWrite(int LearningDegreeWrite) {
        this.LearningDegreeWrite = LearningDegreeWrite;
    }

    public int getLearnedWrite() {
        return LearnedWrite;
    }

    public void setLearnedWrite(int LearnedWrite) {
        this.LearnedWrite = LearnedWrite;
    }
    
    
    
}

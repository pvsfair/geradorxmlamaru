/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 *
 * @author ICCAF
 */
public class SilabaConverter implements Converter {

    @Override
    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext mc) {
        Silaba silaba = (Silaba) o;

        writer.addAttribute("xsi:type", silaba.getTipo());
        writer.setValue(silaba.getSilaba());
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext uc) {
        Silaba silaba = new Silaba();
        
        silaba.setTipo(reader.getAttribute("xsi:type"));
        silaba.setSilaba(reader.getValue());
        
        reader.moveUp();
        
        return silaba;
    }

    @Override
    public boolean canConvert(Class type) {
        return type.equals(Silaba.class);
    }

}

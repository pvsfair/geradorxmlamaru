/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

/**
 *
 * @author ICCAF
 */
public class Usuario {
    private String participante;
    private String responsavel;
    private String login;
    private String senha;

    public Usuario(String participante, String responsavel, String login, String senha) {
        this.participante = participante;
        this.responsavel = responsavel;
        this.login = login;
        this.senha = senha;
    }
    
    /**
     * @return the participante
     */
    public String getParticipante() {
        return participante;
    }

    /**
     * @param participante the participante to set
     */
    public void setParticipante(String participante) {
        this.participante = participante;
    }

    /**
     * @return the responsavel
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public String toString() {
        return participante + " - " + responsavel;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

/**
 *
 * @author ICCAF
 */
public class idEscolha {
    private String tipo;
    private int id;

    public idEscolha() {
    }

    public idEscolha(int id) {
        this.tipo = "xsd:int";
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}

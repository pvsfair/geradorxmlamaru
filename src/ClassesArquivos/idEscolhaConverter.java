/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 *
 * @author ICCAF
 */
public class idEscolhaConverter implements Converter {

    @Override
    public void marshal(Object o, HierarchicalStreamWriter writer, MarshallingContext mc) {
        idEscolha silaba = (idEscolha) o;

        writer.addAttribute("xsi:type", silaba.getTipo());
        writer.setValue(Integer.toString(silaba.getId()));
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext uc) {
        idEscolha silaba = new idEscolha();

        silaba.setTipo(reader.getAttribute("xsi:type"));
        silaba.setId(Integer.parseInt(reader.getValue()));

        reader.moveUp();

        return silaba;
    }

    @Override
    public boolean canConvert(Class type) {
        return type.equals(idEscolha.class);
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClassesArquivos;

/**
 *
 * @author ICCAF
 */
public class Silaba {
    private String tipo;
    private String silaba;

    public Silaba() {
    }

    public Silaba(String silaba) {
        this.tipo = "xsd:string";
        this.silaba = silaba;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSilaba() {
        return silaba;
    }

    public void setSilaba(String silaba) {
        this.silaba = silaba;
    }
    
}
